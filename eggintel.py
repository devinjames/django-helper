import sys
import os
import re
import zipfile


def eggify(env_path, project_path, project_name):
    """

        How should this launch as a sublime text 2 script:
            At the beginning of launch check to see if any isntalled apps have been changed for a project, if they have, run the script.

        Requires: project_name
        Env path??
        we'll seeee

        PSEUDO:
            [x] check the installed apps
            [x] search for the associated eggs (use EGG-INFO\top_level.txt)
            [x] extract the contents (just .py), keep dir structure
            [ ] update the .codeintel/config file if it doesn't exist


    """

    PATH_SITELIB = "Lib\\site-packages"
    libroot = "%s\\%s" % (os.path.abspath(env_path), PATH_SITELIB)
    # projroot = "%s\\%s" % (os.path.abspath(project_path, PATH_SITELIB))

    CODEINTEL_PATH = "%s\\.codeintel" % project_path

    if not os.path.isdir(libroot):
        print "Please enter a complete folder path to the python root"
        return

    if not os.path.isdir(project_path):
        print "Please enter a complete folder path project"
        return

    if not os.path.isdir(CODEINTEL_PATH):
        os.mkdir(CODEINTEL_PATH)
        #TODO: create 'config' file

    # check installed apps

    sys.path.append(project_path)

    import pdb; pdb.set_trace()

    installed_apps = __import__('%s.settings' % project_name, fromlist=['INSTALLED_APPS']).INSTALLED_APPS
    installed_eggs = filter(lambda x: re.match('^django.*', x) is None, installed_apps)

    # find all eggs that are .egg files (not dirs)
    all_eggs = filter(lambda x: os.path.splitext(x)[1].lower()=='.egg' and os.path.isfile(os.path.splitext(x)), os.listdir(libroot))
    # TODO: handle dirs

    # for each egg, if it's installed, extract it to codeintel folder
    for egg in all_eggs:
        z = zipfile.ZipFile("%s\\%s" % (libroot, egg))
        files = z.namelist()
        if 'EGG-INFO/top_level.txt' not in files:
            print "Cannot load '%s', 'EGG-INFO/top_level.txt' does not exist." % egg
            continue

        module_name = z.read('EGG-INFO/top_level.txt').rstrip()

        if module_name not in installed_eggs:
            z.close()
            continue

        py_files = filter(lambda x: re.match('[pPyY]$', x), z.namelist())

        for py_file in py_files:
            z.extract(py_file, "%s\\%s" % (CODEINTEL_PATH, module_name))

        z.close()

if __name__ == "__main__":
    if len(sys.argv) > 1:
        # pass
        eggify(*sys.argv[1:])
    else:
        print "Syntax: eggify.py <path to python root> <path to project> <project name>"

