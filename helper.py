"""
    Django project helper v1.1

    Put these in the root directory where you keep your django projects, adds the following commands:
        dreampie.py - launch a dreampie instance in a virtual python environment
        rs.py [v] - launch a [v]irtual or normal instance of manage.py runserver
        manage.py - run a virtual instance of manage a project
        setenv.py - set a virtual environment as active to perfom actions such as pip install
"""
import os
import sys
import config

PROJECT_DIR = config.PROJECT_DIR
ENV_DIR = config.ENV_DIR
DREAMPIE_PATH = config.DREAMPIE_PATH


class Folder(object):

    def __init__(self, path):
        self.path = path

    def prompt(self, text):
        self.print_folders()
        return raw_input(r"%s " % (text.strip()))

    # def set(self, path):
        # self.set = path

    def folder_list(self):
        os.chdir(self.path)
        f= filter(lambda s: os.path.isdir(s), os.listdir(self.path))
        f.sort()
        return f

    def folder_index(self, num):
        num = int(num)
        x = filter(lambda s: os.path.isdir(s), os.listdir(self.path))
        x.sort()
        if num > len(x):
            print "Index %d out of bounds" % num
            return
        return x[num - 1]

    def print_folders(self):
        i = 1
        for f in self.folder_list():
            print r"%d. %s" % (i, f)
            i += 1

class Environment(Folder):

    def activate(self):
        os.chdir("%s\\%s\\Scripts" % (self.path, self.folder_index(self.prompt("Which environment?"))))
        os.system('cmd /k activate.bat')

    def set_project(self, Project):
        self.project_path = Project.path

    def dreampie(self):
        # self.print_folders()
        env = self.folder_index(self.prompt('Which environment?'))
        (head, tail) = os.path.split(DREAMPIE_PATH)
        os.chdir(head)
        os.system("%s --hide-console-window \"%s\\%s\\Scripts\\python.exe\"" % (tail, self.path, env))

    def manage(self, args):
        if getattr(self, 'project_path') is None:
            print "Requires project path to be set"
            return

	# self.prompt("")
        # self.print_folders()
        f = self.folder_index(self.prompt("Which project?"))
        # f = folder_list(project_num, ENV_DIR)
    	os.chdir("%s\\%s " % (self.project_path, f))
        os.system("%s\\%s\\Scripts\\python.exe \"%s\\%s\\manage.py\" %s" % (self.path, f, self.project_path, f, ' '.join(map(str, args))))

    def runserver(self):
        if getattr(self, 'project_path') is None:
            print "Requires project path to be set"
            return
        proj = self.folder_index(self.prompt("Which environment?"))
        print "\nProject is %s" % proj
        print "Launching runserver ..."
        launchpath = self.path + '\\%s\\Scripts' % proj
        if not os.path.exists(launchpath):
            print "That env could not be found"
            ans = raw_input("Create it? ")
            if ans.lower() == 'y':
                os.system('mkvirtualenv %s' % proj)
            # return

        os.chdir(self.path + '\\%s\\Scripts' % proj)
        try:
            os.system(r'python.exe %s\manage.py runserver' % (self.project_path + '\\' + proj))
        except KeyboardInterrupt:
            print "Received keyboard interrupt, closing."


class Project(Folder):

    def activate(self):
        proj = self.folder_index(self.prompt("Which project?"))
        os.environ['DJANGO_SETTINGS_MODULE'] = "%s.settings" % proj
        print "Project is %s" % proj

    def runserver(self):
        proj = self.folder_index(self.prompt("Which project?"))
        print "\nProject is %s" % proj
        print "Launching runserver ..."
        os.chdir(self.path + '\\' + proj)
        try:
            os.system('python manage.py runserver')
        except KeyboardInterrupt:
            print "Received keyboard interrupt, closing."

proj = Project(PROJECT_DIR)
env = Environment(ENV_DIR)
env.set_project(proj)

if __name__ == "__main__":
    if len(sys.argv) > 1:
        print "Helper takes 0 arguments, to be loaded as a module"
    else:
        proj.activate()
