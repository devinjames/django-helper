import helper
import os
import sys

if __name__ == "__main__":
    if len(sys.argv) > 1:
        # For launching in a virtual environment
        if sys.argv[1].lower() == 'v':
            helper.env.runserver()
    else:
        helper.proj.runserver()
