Requires:
Virtualenvwrapper https://github.com/davidmarble/virtualenvwrapper-win

Put these files anywhere available in your system PATH.

SETUP:
-----

Edit helper.py and edit the appropriate paths.

USAGE:
-----

Commands are:

**rs.py [v]**

  Lists all projects in project path, inititiations runserver command to numerical index entered.  Accepts a single optional parameter 'v' to start the server in a virtualenv.

**setenv.py**

	Simplifies settings the active virtualenv, requires virtualenvwrapper

**manage.py [args]**

	Executes manage.py for a given django project in the virtual environment as is selected from the numerical index.
